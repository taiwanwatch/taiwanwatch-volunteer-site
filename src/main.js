/* eslint-disable no-new, no-unused-vars */

import Vue from 'vue'
import axios from 'axios'
import i18n from './utils/i18nManager'
import { sync } from 'vuex-router-sync'
import router from './utils/routerManager'
import store from './store'
import awsConfig from '../config/aws'
import iView from 'iview'
// FB SDK
import '@/utils/fbSdk'
// Styles
import 'iview/dist/styles/iview.css'
import '@/assets/styles/main.css'
import '@/assets/styles/markdown.scss'

Vue.use(iView, {
  // fix from http://element.eleme.io/2.0/#/en-US/component/i18n#compatible-with-2
  i18n: (key, value) => i18n.t(key, value)
})
Vue.config.productionTip = false
axios.defaults.baseURL = awsConfig.api.ENDPOINT

// returns an unsync callback fn
const unsync = sync(store, router)

// load fb sdk and check if the user has logged in
store.dispatch('FB_LOAD_SDK', {})

new Vue({
  el: '#app',
  router,
  i18n,
  store,
  template: '<router-view></router-view>'
})
