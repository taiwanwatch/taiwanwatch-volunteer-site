import Vue from 'vue'
import VueI18n from 'vue-i18n'

// i18n from modules
import enApp from '@/modules/App/i18n/en-US.json'
import zhApp from '@/modules/App/i18n/zh-TW.json'
import enHome from '@/modules/Home/i18n/en-US.json'
import zhHome from '@/modules/Home/i18n/zh-TW.json'
import enCategorize from '@/modules/BillCategorize/i18n/en-US.json'
import zhCategorize from '@/modules/BillCategorize/i18n/zh-TW.json'
import enDownload from '@/modules/BillDownload/i18n/en-US.json'
import zhDownload from '@/modules/BillDownload/i18n/zh-TW.json'
import enManagement from '@/modules/BillManagement/i18n/en-US.json'
import zhManagement from '@/modules/BillManagement/i18n/zh-TW.json'
// import i18n texts for iView
import enIview from 'iview/src/locale/lang/en-US'
import zhIview from 'iview/src/locale/lang/zh-TW'

Vue.use(VueI18n)

export default new VueI18n({
  locale: 'en-US',
  fallbackLocale: 'en-US',
  messages: {
    'en-US': {
      ...enApp,
      ...enHome,
      ...enCategorize,
      ...enDownload,
      ...enManagement,
      ...enIview
    },
    'zh-TW': {
      ...zhApp,
      ...zhHome,
      ...zhCategorize,
      ...zhDownload,
      ...zhManagement,
      ...zhIview
    }
  }
})
