// const getToken = () => {
//   return localStorage.token
// }

const isAuthenticated = () => {
  return !!localStorage.token
}

const requireAuth = (to, from, next) => {
  if (!isAuthenticated()) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
  } else {
    next()
  }
}

export default requireAuth
