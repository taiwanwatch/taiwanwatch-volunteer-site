import Vue from 'vue'
import VueRouter from 'vue-router'

import AppRoute from '@/modules/App/router'

Vue.use(VueRouter)

export default new VueRouter({
  scrollBehavior: () => ({ y: 0 }),
  routes: [AppRoute]
})
