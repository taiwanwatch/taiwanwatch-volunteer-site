/* global FB */
import store from '@/store'
import axios from 'axios'
import sigV4Client from '@/utils/sigV4Client'
import awsConfig from '../../config/aws.json'

// GENERAL

export const getAwsSignedRequest = (awsCredential, originalRequest) => {
  const signedRequest = sigV4Client
    .newClient({
      accessKey: awsCredential.accessKeyId,
      secretKey: awsCredential.secretAccessKey,
      sessionToken: awsCredential.sessionToken,
      region: awsConfig.metadata.REGION,
      endpoint: awsConfig.api.ENDPOINT
    })
    .signRequest(originalRequest)

  return signedRequest
}

export const waitForAwsCredsReady = () => {
  const tryNumMax = 60
  let tryNum = 0

  return new Promise((resolve, reject) => {
    let check = setInterval(() => {
      tryNum++
      if (store.getters.awsCredential.set) {
        clearInterval(check)
        check = false
        resolve()
      }
      if (tryNum === tryNumMax) {
        reject(new Error('CHECKING_AWS_CREDS_TIMEOUT'))
      }
    }, 500)
  })
}

// LOGIN

export const getFbUserInfo = () => {
  return new Promise((resolve, reject) => {
    FB.api('/me?fields=name,id,email', function (response) {
      if (!response || response.error) {
        const errorMsg = response.error || 'cannot get facebook user info'
        reject(errorMsg)
      } else {
        resolve(response)
      }
    })
  })
}

export const logon = params => {
  return axios
    .post('users/logon', params)
    .then(response => Promise.resolve(response))
    .catch(error => Promise.reject(error))
}

// DASHBOARD

export const getDashboardInfo = () => {
  const originalRequest = {
    path: '/dashboard/userRecord',
    method: 'GET',
    headers: {},
    queryParams: {}
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'GET',
    url: signedRequest.url,
    headers: signedRequest.headers
  })
    .then(response => Promise.resolve(response))
    .catch(error => Promise.reject(error))
}

// BILL DOWNLOAD

export const getBillUploadUrl = ({ contentType, billId, billType, billNumber, billVersion, versionDate, congress }) => {
  const body = {
    contentType,
    billId,
    billType,
    billNumber,
    billVersion,
    versionDate,
    congress
  }
  const originalRequest = {
    path: '/billDownload/uploadUrl',
    method: 'POST',
    headers: {},
    queryParams: {},
    body
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'POST',
    url: signedRequest.url,
    headers: signedRequest.headers,
    data: body
  })
    .then(response => {
      return Promise.resolve(response.data.url || '/')
    })
    .catch(error => Promise.reject(new Error(error)))
}

// BILL MANAGEMENT

export const getBills = async () => {
  return axios({
    method: 'GET',
    url: '/billManagement/bills',
    headers: {}
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}

export const getBillTypes = async () => {
  const originalRequest = {
    path: '/billManagement/billTypes',
    method: 'GET',
    headers: {},
    queryParams: {}
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'GET',
    url: signedRequest.url,
    headers: signedRequest.headers
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}

export const getBillActions = async () => {
  const originalRequest = {
    path: '/billManagement/billActions',
    method: 'GET',
    headers: {},
    queryParams: {}
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'GET',
    url: signedRequest.url,
    headers: signedRequest.headers
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}

export const getBillVersions = async () => {
  const originalRequest = {
    path: '/billManagement/billVersions',
    method: 'GET',
    headers: {},
    queryParams: {}
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'GET',
    url: signedRequest.url,
    headers: signedRequest.headers
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}

export const getBillCategories = async () => {
  const originalRequest = {
    path: '/billManagement/billCategories',
    method: 'GET',
    headers: {},
    queryParams: {}
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'GET',
    url: signedRequest.url,
    headers: signedRequest.headers
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}

export const getSponsorSuggestList = async query => {
  const body = {
    query: {
      name: query
    }
  }
  const originalRequest = {
    path: '/billManagement/sponsors',
    method: 'POST',
    headers: {},
    queryParams: {},
    body
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'POST',
    url: signedRequest.url,
    headers: signedRequest.headers,
    data: body
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}

export const createBill = async bill => {
  const body = {
    bill
  }
  const originalRequest = {
    path: '/billManagement/bills',
    method: 'PUT',
    headers: {},
    queryParams: {},
    body
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'PUT',
    url: signedRequest.url,
    headers: signedRequest.headers,
    data: body
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}

export const updateCoSponsors = async ({ billId, cosponsors }) => {
  const body = {
    billId,
    cosponsors
  }
  const originalRequest = {
    path: '/billManagement/cosponsors',
    method: 'POST',
    headers: {},
    queryParams: {},
    body
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'POST',
    url: signedRequest.url,
    headers: signedRequest.headers,
    data: body
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}

export const addCategory = async ({ billId, category }) => {
  const body = {
    billId,
    category
  }
  const originalRequest = {
    path: '/billManagement/addBillCategory',
    method: 'PUT',
    headers: {},
    queryParams: {},
    body
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'PUT',
    url: signedRequest.url,
    headers: signedRequest.headers,
    data: body
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}

export const removeCategory = async ({ billId, categoryId }) => {
  const body = {
    billId,
    categoryId
  }
  const originalRequest = {
    path: '/billManagement/removeBillCategory',
    method: 'DELETE',
    headers: {},
    queryParams: {},
    body
  }
  const signedRequest = getAwsSignedRequest(store.getters.awsCredential, originalRequest)
  return axios({
    method: 'DELETE',
    url: signedRequest.url,
    headers: signedRequest.headers,
    data: body
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}

export const getBillKeywords = async ({ pdfUrl }) => {
  const body = {
    api_key: '5566520forever',
    isPDFUrl: true,
    url: pdfUrl,
    keygenUsePrepFlash: true,
    keygenUseNER: true,
    keygenUseSmile: true,
    runKeywordOnly: true
  }
  return axios({
    method: 'DELETE',
    url: 'http://34.225.235.218:8080/engine',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: body
  })
    .then(response => Promise.resolve(response.data))
    .catch(error => Promise.reject(error))
}
