/* global FB */
import router from '@/utils/routerManager'
import {
  getFbUserInfo,
  logon,
  getDashboardInfo,
  getBills,
  getBillTypes,
  getBillActions,
  getBillVersions,
  getBillCategories,
  getSponsorSuggestList,
  createBill,
  updateCoSponsors,
  addCategory,
  removeCategory,
  getBillKeywords
} from '@/api'

export default {
  FB_LOAD_SDK ({ commit, dispatch, state }, payload) {
    commit('START_AUTH')
    window.fbAsyncInit = function () {
      FB.init({
        appId: '1984985615071292',
        cookie: true,
        xfbml: true,
        version: 'v2.8'
      })
      FB.AppEvents.logPageView()
      dispatch('FB_CHECK_STATUS', {})
    }
  },
  FB_CHECK_STATUS ({ commit, dispatch, state }, payload) {
    FB.getLoginStatus(response => {
      dispatch('UPDATE_USER', { fbResponse: response })
    })
  },
  FB_LOGIN ({ commit, dispatch, state }, { options }) {
    FB.login(function (response) {
      localStorage.token = response.authResponse.accessToken
      dispatch('UPDATE_USER', { fbResponse: response })
      router.replace(state.route.query.redirect || '/')
    }, options)
  },
  FB_LOGOUT ({ commit, dispatch, state }) {
    FB.logout(function (response) {
      delete localStorage.token
      dispatch('UPDATE_USER', { fbResponse: response })
      router.replace('/login')
    })
  },
  async UPDATE_USER ({ commit, dispatch, state }, { fbResponse }) {
    const authenticated = fbResponse.status === 'connected'
    commit('SET_AUTH', { authenticated })

    if (authenticated) {
      getFbUserInfo()
        .then(userInfo =>
          logon({
            fbAccessToken: fbResponse.authResponse.accessToken,
            fbUserId: fbResponse.authResponse.userID,
            name: userInfo.name,
            email: userInfo.email
          })
        )
        .then(response => {
          // set user's credential
          commit('SET_AWS_CREDENTIAL', { awsCredential: response.data.data.credentail })
          // set user's info
          commit('SET_USER', { user: response.data.data.user })
          // get latest dashboard info for the user
          dispatch('GET_DASHBOARD_INFO')
        })
        .catch(error => {
          console.log('cannot logon: ', error)
          dispatch('FB_LOGOUT')
        })
    } else {
      commit('SET_USER', { user: {} })
    }
  },
  async GET_DASHBOARD_INFO () {
    await getDashboardInfo()
      .then(data => {
        console.log('mmm', data)
      })
      .catch(error => {
        console.log('nnn', error)
      })
  },
  // BILL MANAGEMENT
  async GET_BILLS ({ commit }) {
    commit('LOAD_BILLS', {})
    try {
      let bills = await getBills()
      commit('SET_BILLS', { bills })
    } catch (error) {
      console.log(error)
    }
  },
  async GET_BILL_TYPES ({ commit }) {
    try {
      let billTypes = await getBillTypes()
      commit('SET_BILL_TYPES', { billTypes })
    } catch (error) {
      console.log(error)
    }
  },
  async GET_BILL_ACTIONS ({ commit }) {
    try {
      let billActions = await getBillActions()
      commit('SET_BILL_ACTIONS', { billActions })
    } catch (error) {
      console.log(error)
    }
  },
  async GET_BILL_VERSIONS ({ commit }) {
    try {
      let billVersions = await getBillVersions()
      commit('SET_BILL_VERSIONS', { billVersions })
    } catch (error) {
      console.log(error)
    }
  },
  async GET_BILL_CATEGORIES ({ commit }) {
    try {
      let billCategories = await getBillCategories()
      commit('SET_BILL_CATEGORIES', { billCategories })
    } catch (error) {
      console.log(error)
    }
  },
  async GET_SPONSOR_SUGGEST_LIST ({ commit }, { query }) {
    commit('LOAD_SPONSOR_SUGGEST_LIST', {})
    try {
      let sponsorSuggestList = await getSponsorSuggestList(query)
      commit('SET_SPONSOR_SUGGEST_LIST', { sponsorSuggestList })
    } catch (error) {
      console.log(error)
    }
  },
  async GET_COSPONSOR_SUGGEST_LIST ({ commit }, { query }) {
    commit('LOAD_COSPONSOR_SUGGEST_LIST', {})
    try {
      let coSponsorSuggestList = await getSponsorSuggestList(query)
      commit('SET_COSPONSOR_SUGGEST_LIST', { coSponsorSuggestList })
    } catch (error) {
      console.log(error)
    }
  },
  async CREATE_BILL ({ commit }, { bill }) {
    commit('START_CREATING_BILL', {})
    try {
      let createdBill = await createBill(bill)
      console.log('created bill:', createdBill)
      commit('FINISH_CREATING_BILL', {})
    } catch (error) {
      console.log(error)
    }
  },
  async UPDATE_COSPONSORS ({ commit }, { billId, cosponsors }) {
    commit('START_SUBMITTING_COSPONSORS', {})
    try {
      let updatedCosponsors = await updateCoSponsors({ billId, cosponsors })
      console.log('updated co-sponsors:', updatedCosponsors)
      commit('FINISH_SUBMITTING_COSPONSORS', {})
    } catch (error) {
      console.log(error)
    }
  },
  async ADD_CATEGORY ({ commit }, { billId, category }) {
    commit('START_ADDING_CATEGORY', {})
    try {
      let updatedBill = await addCategory({ billId, category })
      console.log('updated bill:', updatedBill)
      commit('SET_BILLCATEGORIZE_SELECTEDBILL', { selectedBill: updatedBill })
      commit('FINISH_ADDING_CATEGORY', {})
    } catch (error) {
      console.log(error)
    }
  },
  async REMOVE_CATEGORY ({ commit }, { billId, categoryId }) {
    commit('START_ADDING_CATEGORY', {})
    try {
      let updatedBill = await removeCategory({ billId, categoryId })
      console.log('updated bill:', updatedBill)
      commit('SET_BILLCATEGORIZE_SELECTEDBILL', { selectedBill: updatedBill })
      commit('FINISH_ADDING_CATEGORY', {})
    } catch (error) {
      console.log(error)
    }
  },
  // BILL CATEGORIZE
  async GET_BILL_KEYWORDS ({ commit }, { pdfUrl }) {
    commit('START_ADDING_CATEGORY', {})
    try {
      let keywords = await getBillKeywords({ pdfUrl })
      console.log('keywords:', keywords)
      commit('SET_SUGGESTED_KEYWORDS', { suggestedKeywords: keywords })
      commit('FINISH_ADDING_CATEGORY', {})
    } catch (error) {
      console.log(error)
    }
  }
}
