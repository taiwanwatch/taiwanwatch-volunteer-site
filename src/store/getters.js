export default {
  userInfo (state, getters) {
    return {
      id: state.user.id,
      imgUrl: state.user.id ? `https://graph.facebook.com/${state.user.fbUserId}/picture?width=300` : '',
      name: state.user.name ? state.user.name : 'user'
    }
  },
  awsCredential (state, getters) {
    return state.awsCredential
  }
}
