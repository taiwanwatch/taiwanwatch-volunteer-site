import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isAuthenticated: false,
    authenticating: false,
    user: {
      id: ''
    },
    awsCredential: {
      set: false
    },
    billManagement: {
      bills: [],
      selectedBill: null,
      billsLoading: false,
      billTypes: [],
      billActions: [],
      billVersions: [],
      billCategories: [],
      sponsorSuggestList: [],
      coSponsorSuggestList: [],
      sponsorSuggestListLoading: false,
      coSponsorSuggestListLoading: false,
      createBillFormSubmitting: false,
      addCoSponsorFormSubmitting: false,
      addCategoryFormSubmitting: false
    },
    billCategorize: {
      selectedBill: null,
      suggestedKeywords: []
    },
    billDownload: {
      selectedBill: null,
      selectedFiles: [],
      fileUploaded: false,
      fileUploading: false
    }
  },
  actions,
  mutations,
  getters
})
