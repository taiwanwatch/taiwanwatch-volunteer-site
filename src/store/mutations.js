export default {
  // LOGIN
  START_AUTH (state) {
    state.isAuthenticated = false
    state.authenticating = true
  },
  SET_AUTH (state, { authenticated }) {
    state.isAuthenticated = authenticated
    state.authenticating = false
  },
  SET_USER (state, { user }) {
    state.user = {
      ...state.user,
      id: user.id ? user.id : '',
      fbUserId: user.fbUserId ? user.fbUserId : '',
      name: user.name ? user.name : '',
      email: user.email ? user.email : ''
    }
  },
  SET_AWS_CREDENTIAL (state, { awsCredential }) {
    state.awsCredential = {
      ...state.awsCredential,
      set: true,
      ...awsCredential
    }
  },
  // BILL CATEGORIZE
  SET_BILLCATEGORIZE_SELECTEDBILL (state, { selectedBill }) {
    state.billCategorize.selectedBill = selectedBill
  },
  RESET_BILLCATEGORIZE_TASK (state, payload) {
    state.billCategorize.selectedBill = null
    state.billCategorize.formSubmitted = false
  },
  SET_SUGGESTED_KEYWORDS (state, { suggestedKeywords }) {
    state.billCategorize.suggestedKeywords = suggestedKeywords
  },
  // BILL DOWNLOAD
  SET_BILLDOWNLOAD_SELECTEDBILL (state, { selectedBill }) {
    state.billDownload.selectedBill = selectedBill
  },
  SET_BILLDOWNLOAD_SELECTEDFILES (state, { selectedFile }) {
    state.billDownload.selectedFiles.push(selectedFile)
  },
  SET_BILLDOWNLOAD_UPLOADSTARTED (state, payload) {
    state.billDownload.fileUploading = true
  },
  SET_BILLDOWNLOAD_UPLOADFINISHED (state, { uploaded }) {
    state.billDownload.fileUploading = false
    state.billDownload.fileUploaded = uploaded
  },
  RESET_BILLDOWNLOAD_TASK (state, payload) {
    state.billDownload.selectedBill = null
    state.billDownload.selectedFiles = []
    state.billDownload.fileUploaded = false
    state.billDownload.fileUploading = false
  },
  // BILL MANAGEMENT
  LOAD_BILLS (state, payload) {
    state.billManagement.billsLoading = true
  },
  SET_BILLS (state, { bills }) {
    state.billManagement.bills = bills
    state.billManagement.billsLoading = false
  },
  RESET_BILLS (state, payload) {
    state.billManagement.bills = []
  },
  SET_SELECTEDBILL (state, { selectedBill }) {
    state.billManagement.selectedBill = selectedBill
  },
  SET_BILL_TYPES (state, { billTypes }) {
    state.billManagement.billTypes = billTypes
  },
  SET_BILL_ACTIONS (state, { billActions }) {
    state.billManagement.billActions = billActions
  },
  SET_BILL_VERSIONS (state, { billVersions }) {
    state.billManagement.billVersions = billVersions
  },
  SET_BILL_CATEGORIES (state, { billCategories }) {
    state.billManagement.billCategories = billCategories
  },
  LOAD_SPONSOR_SUGGEST_LIST (state, payload) {
    state.billManagement.sponsorSuggestListLoading = true
  },
  LOAD_COSPONSOR_SUGGEST_LIST (state, payload) {
    state.billManagement.coSponsorSuggestListLoading = true
  },
  SET_SPONSOR_SUGGEST_LIST (state, { sponsorSuggestList }) {
    state.billManagement.sponsorSuggestListLoading = false
    state.billManagement.sponsorSuggestList = sponsorSuggestList
  },
  SET_COSPONSOR_SUGGEST_LIST (state, { coSponsorSuggestList }) {
    state.billManagement.coSponsorSuggestListLoading = false
    state.billManagement.coSponsorSuggestList = coSponsorSuggestList
  },
  START_CREATING_BILL (state, payload) {
    state.billManagement.createBillFormSubmitting = true
  },
  FINISH_CREATING_BILL (state, payload) {
    state.billManagement.createBillFormSubmitting = false
  },
  START_SUBMITTING_COSPONSORS (state, payload) {
    state.billManagement.addCoSponsorFormSubmitting = true
  },
  FINISH_SUBMITTING_COSPONSORS (state, payload) {
    state.billManagement.addCoSponsorFormSubmitting = false
  },
  START_ADDING_CATEGORY (state, payload) {
    state.billManagement.addCategoryFormSubmitting = true
  },
  FINISH_ADDING_CATEGORY (state, payload) {
    state.billManagement.addCategoryFormSubmitting = false
  },
  RESET_BILLMANAGEMENT_TASK (state, payload) {
    state.billManagement.bills = []
    state.billManagement.selectedBill = null
  }
}
