const BillCategorizeLayout = () => import('@/modules/BillCategorize/views/Layout')
const BillCategorizeDocView = () => import('@/modules/BillCategorize/views/Doc')
const BillCategorizeWorkView = () => import('@/modules/BillCategorize/views/Work')

const BillCategorizeRoute = {
  path: 'categorize',
  redirect: 'categorize/doc',
  component: BillCategorizeLayout,
  name: 'BillCategorizeLayout',
  meta: {description: 'Bill categorization'},
  children: [
    {
      path: 'doc',
      component: BillCategorizeDocView,
      name: 'BillCategorizeDocView',
      meta: {description: 'Bill categorization documentation'}
    },
    {
      path: 'work',
      component: BillCategorizeWorkView,
      name: 'BillCategorizeWorkView',
      meta: {description: 'Bill categorization workspace'}
    }
  ]
}

export default BillCategorizeRoute
