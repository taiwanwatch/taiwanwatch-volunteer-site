const BillRelevanceLayout = () => import('@/modules/BillRelevance/views/Layout')
const BillRelevanceDocView = () => import('@/modules/BillRelevance/views/Doc')
const BillRelevanceWorkView = () => import('@/modules/BillRelevance/views/Work')

const BillRelevanceRoute = {
  path: 'relevance',
  redirect: 'relevance/doc',
  component: BillRelevanceLayout,
  name: 'BillRelevanceLayout',
  meta: {description: 'Bill relevance'},
  children: [
    {
      path: 'doc',
      component: BillRelevanceDocView,
      name: 'BillRelevanceDocView',
      meta: {description: 'Bill relevance documentation'}
    },
    {
      path: 'work',
      component: BillRelevanceWorkView,
      name: 'BillRelevanceWorkView',
      meta: {description: 'Bill relevance workspace'}
    }
  ]
}

export default BillRelevanceRoute
