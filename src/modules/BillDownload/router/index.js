const BillDownloadLayout = () => import('@/modules/BillDownload/views/Layout')
const BillDownloadDocView = () => import('@/modules/BillDownload/views/Doc')
const BillDownloadWorkView = () => import('@/modules/BillDownload/views/Work')

const BillDownloadRoute = {
  path: 'download',
  redirect: 'download/doc',
  component: BillDownloadLayout,
  name: 'BillDownloadLayout',
  meta: {description: 'Bill download'},
  children: [
    {
      path: 'doc',
      component: BillDownloadDocView,
      name: 'BillDownloadDocView',
      meta: {description: 'Bill download documentation'}
    },
    {
      path: 'work',
      component: BillDownloadWorkView,
      name: 'BillDownloadWorkView',
      meta: {description: 'Bill download workspace'}
    }
  ]
}

export default BillDownloadRoute
