## Intro 🤓
---
This task is to help download the PDF files of listed Taiwan-related bills from several source sites. Although Congress bill information is vastly available through various APIs provided by online open source projects, having the full-text of the bills always better enable and facilitate the analysis work.

<br>

## Reward 🏆
---
Each version of a bill downloaded will give you `1` point. Usually a bill will have multiple versions so you can expect to earn multiple points when finish downloading PDF files for one bill 😆

<br>

## Congress Bill 101 📚
---
Before you start, some prerequisite knowledge is preferable to avoid confusion. If you are already familiar with these, feel free to skip this part and come back later when needed.

### Bill type
There are `4` kinds of bill in the U.S. Congress and each for both `House` and `Senate`. Therefore, in totally you will see `8` different types of bill:

- **H.R.** ➡️ House Bill
- **S.** ➡️ Senate Bill
- **H.J. Res.** ➡️ House Joint Resolution
- **S.J. Res.** ➡️ Senate Joint Resolution
- **H. Con. Res.** ➡️ House Concurrent Resolution
- **S. Con. Res.** ➡️ Senate Concurrent Resolution
- **H. Res.** ➡️ House Simple Resolution
- **S. Res.** ➡️ Senate Simple Resolution

### Bill code
Besides bill type, each bill will be assigned with a number and bill code is the combination of `bill type` and `bill number`. Take `H.R. 3320` for example. `H.R.` is its bill type and `3320` is the bill number.

### Congress
Each term of Congress lasts for 2 years and has 2 sessions. The current Congress is `115th` which began from January 3, 2017 and will end on January 3, 2019.

### Version
There are numerous different bill versions that track a bill through the legislative process from introduction through passage by both chambers (enrolled version).


The following are values for bill version:
```
$versions = array(
  'ash'     => t('Additional Sponsors House'),
  'ath'     => t('Agreed to House'),
  'ats'     => t('Agreed to Senate'),
  'cdh'     => t('Committee Discharged House'),
  'cds'     => t('Committee Discharged Senate'),
  'cph'     => t('Considered and Passed House'),
  'cps'     => t('Considered and Passed Senate'),
  'eah'     => t('Engrossed Amendment House'),
  'eas'     => t('Engrossed Amendment Senate'),
  'eh'      => t('Engrossed in House'),
  'ehr'     => t('Engrossed in House-Reprint'),
  'eh_s'    => t('Engrossed in House (No.) Star Print [*]'),
  'enr'     => t('Enrolled Bill'),
  'es'      => t('Engrossed in Senate'),
  'esr'     => t('Engrossed in Senate-Reprint'),
  'es_s'    => t('Engrossed in Senate (No.) Star Print'),
  'fah'     => t('Failed Amendment House'),
  'fps'     => t('Failed Passage Senate'),
  'hdh'     => t('Held at Desk House'),
  'hds'     => t('Held at Desk Senate'),
  'ih'      => t('Introduced in House'),
  'ihr'     => t('Introduced in House-Reprint'),
  'ih_s'    => t('Introduced in House (No.) Star Print'),
  'iph'     => t('Indefinitely Postponed in House'),
  'ips'     => t('Indefinitely Postponed in Senate'),
  'is'      => t('Introduced in Senate'),
  'isr'     => t('Introduced in Senate-Reprint'),
  'is_s'    => t('Introduced in Senate (No.) Star Print'),
  'lth'     => t('Laid on Table in House'),
  'lts'     => t('Laid on Table in Senate'),
  'oph'     => t('Ordered to be Printed House'),
  'ops'     => t('Ordered to be Printed Senate'),
  'pch'     => t('Placed on Calendar House'),
  'pcs'     => t('Placed on Calendar Senate'),
  'pp'      => t('Public Print'),
  'rah'     => t('Referred w/Amendments House'),
  'ras'     => t('Referred w/Amendments Senate'),
  'rch'     => t('Reference Change House'),
  'rcs'     => t('Reference Change Senate'),
  'rdh'     => t('Received in House'),
  'rds'     => t('Received in Senate'),
  're'      => t('Reprint of an Amendment'),
  'reah'    => t('Re-engrossed Amendment House'),
  'renr'    => t('Re-enrolled'),
  'res'     => t('Re-engrossed Amendment Senate'),
  'rfh'     => t('Referred in House'),
  'rfhr'    => t('Referred in House-Reprint'),
  'rfh_s'   => t('Referred in House (No.) Star Print'),
  'rfs'     => t('Referred in Senate'),
  'rfsr'    => t('Referred in Senate-Reprint'),
  'rfs_s'   => t('Referred in Senate (No.) Star Print'),
  'rh'      => t('Reported in House'),
  'rhr'     => t('Reported in House-Reprint'),
  'rh_s'    => t('Reported in House (No.) Star Print'),
  'rih'     => t('Referral Instructions House'),
  'ris'     => t('Referral Instructions Senate'),
  'rs'      => t('Reported in Senate'),
  'rsr'     => t('Reported in Senate-Reprint'),
  'rs_s'    => t('Reported in Senate (No.) Star Print'),
  'rth'     => t('Referred to Committee House'),
  'rts'     => t('Referred to Committee Senate'),
  'sas'     => t('Additional Sponsors Senate'),
  'sc'      => t('Sponsor Change House'),
  's_p'     => t('Star (No.) Print of an Amendment'),
);
```

<br>

## Steps 💻
---

### One Version

<img src="static/BillDownload/docs/one-version.png" width="500px">

### Multiple Versions

<img src="static/BillDownload/docs/multiple-versions.png" width="500px">
