export const getSelectCol = vm => {
  return {
    title: ' ',
    key: 'select',
    className: 'tableRowSelect',
    width: 60,
    render: (h, params) => {
      return h('Icon', {
        class: params.index === vm.selectedTableRowIndex ? 'selected' : 'unselected',
        props: { type: 'checkmark' }
      })
    }
  }
}

export const getCongressCol = vm => {
  return {
    title: vm.$t('billManagement.table.billCongressColLabel'),
    key: 'congress',
    width: 110,
    render: (h, params) => {
      return h(
        'Tag',
        {
          props: { type: 'dot', color: params.row.congress !== 115 ? 'red' : 'green' }
        },
        params.row.congress
      )
    }
  }
}

export const getBillCodeCol = vm => {
  return {
    title: vm.$t('billManagement.table.billCodeColLabel'),
    key: 'billCode',
    width: 120,
    className: 'small',
    render: (h, params) => {
      return h('div', `${params.row.billType.display} ${params.row.billNumber}`)
    }
  }
}

export const getTitleCol = vm => {
  return {
    title: vm.$t('billManagement.table.billTitleColLabel'),
    key: 'title',
    className: 'tableBillTitleCol'
  }
}

export const getVersionsCol = vm => {
  return {
    title: vm.$t('billManagement.table.billVersionsColLabel'),
    key: 'versions',
    width: 100,
    render: (h, params) => {
      if (!params.row.versions || params.row.versions.length === 0) {
        return h('div', 'none')
      }
      let versionList = h(
        'ul',
        params.row.versions.map(item => {
          return h(
            'li',
            {
              style: { textAlign: 'left', padding: '4px' }
            },
            item.name
          )
        })
      )

      return h(
        'Poptip',
        {
          props: {
            trigger: 'hover',
            title: `${params.row.versions.length} ${vm.$t('billManagement.table.billVersionsPopTitleUnit')}`,
            placement: 'bottom'
          }
        },
        [h('Tag', params.row.versions.length), h('div', { slot: 'content' }, [versionList])]
      )
    }
  }
}

export const getLastUpdatedCol = vm => {
  return {
    title: vm.$t('billManagement.table.billLastUpdatedColLabel'),
    key: 'lastUpdated',
    width: 120,
    render: (h, params) => {
      return h('div', params.row.lastUpdated)
    }
  }
}
