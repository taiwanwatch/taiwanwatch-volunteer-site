const LoginView = () => import('@/modules/Login/views/Login')

const LoginRoute = {
  path: 'login',
  component: LoginView,
  name: 'LoginView',
  meta: {description: 'Login page'}
}

export default LoginRoute
