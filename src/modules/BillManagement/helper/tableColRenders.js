export const getSelectCol = vm => {
  return {
    title: ' ',
    key: 'select',
    className: 'tableRowSelect',
    width: 60,
    render: (h, params) => {
      return h('Icon', {
        class: params.index === vm.selectedTableRowIndex ? 'selected' : 'unselected',
        props: { type: 'checkmark' }
      })
    }
  }
}

export const getCongressCol = vm => {
  return {
    title: vm.$t('billManagement.table.billCongressColLabel'),
    key: 'congress',
    sortable: true,
    sortType: 'asc',
    width: 110,
    render: (h, params) => {
      return h(
        'Tag',
        {
          props: { type: 'dot', color: params.row.congress !== 115 ? 'red' : 'green' }
        },
        params.row.congress
      )
    }
  }
}

export const getBillCodeCol = vm => {
  return {
    title: vm.$t('billManagement.table.billCodeColLabel'),
    key: 'billCode',
    width: 120,
    className: 'small',
    render: (h, params) => {
      return h('div', `${params.row.billType.display} ${params.row.billNumber}`)
    }
  }
}

export const getTitleCol = vm => {
  return {
    title: vm.$t('billManagement.table.billTitleColLabel'),
    key: 'title',
    className: 'tableBillTitleCol'
  }
}

export const getChamberCol = vm => {
  return {
    title: vm.$t('billManagement.table.billChamberColLabel'),
    key: 'chamber',
    width: 100,
    render: (h, params) => {
      return h('div', params.row.billType.chamber)
    }
  }
}

export const getVersionsCol = vm => {
  return {
    title: vm.$t('billManagement.table.billVersionsColLabel'),
    key: 'versions',
    width: 100,
    render: (h, params) => {
      let versionList = h(
        'ul',
        vm.tableData[params.index].versions.map(item => {
          return h(
            'li',
            {
              style: { textAlign: 'left', padding: '4px' }
            },
            item.name
          )
        })
      )

      return h(
        'Poptip',
        {
          props: {
            trigger: 'hover',
            title: `${params.row.versions.length} ${vm.$t('billManagement.table.billVersionsPopTitleUnit')}`,
            placement: 'bottom'
          }
        },
        [h('Tag', params.row.versions.length), h('div', { slot: 'content' }, [versionList])]
      )
    }
  }
}

export const getSponsorCol = vm => {
  return {
    title: vm.$t('billManagement.table.billSponsorColLabel'),
    key: 'sponsor',
    width: 120,
    render: (h, params) => {
      return h('div', `${params.row.sponsor.name}`)
    }
  }
}

export const getLastUpdatedCol = vm => {
  return {
    title: vm.$t('billManagement.table.billLastUpdatedColLabel'),
    key: 'lastUpdated',
    width: 120,
    render: (h, params) => {
      return h('div', params.row.lastUpdated)
    }
  }
}

// const formatDate = sDate => {
//   let date = new Date(sDate)
//   const y = date.getFullYear()
//   let m = date.getMonth() + 1
//   m = m < 10 ? '0' + m : m
//   let d = date.getDate()
//   d = d < 10 ? '0' + d : d
//   return y + '-' + m + '-' + d
// }
