const BillManagementLayout = () => import('@/modules/BillManagement/views/Layout')
const BillManagementDocView = () => import('@/modules/BillManagement/views/Doc')
const BillManagementWorkView = () => import('@/modules/BillManagement/views/Work')

const BillManagementRoute = {
  path: 'management',
  redirect: 'management/doc',
  component: BillManagementLayout,
  name: 'BillManagementLayout',
  meta: { description: 'Bill management' },
  children: [
    {
      path: 'doc',
      component: BillManagementDocView,
      name: 'BillManagementDocView',
      meta: { description: 'Bill management documentation' }
    },
    {
      path: 'work',
      component: BillManagementWorkView,
      name: 'BillManagementWorkView',
      meta: { description: 'Bill management workspace' }
    }
  ]
}

export default BillManagementRoute
