import LoginRoute from '@/modules/Login/router'
import HomeRoute from '@/modules/Home/router'

const AppLayout = () => import('@/modules/App/views/Layout')

const AppRoute = {
  path: '/',
  component: AppLayout,
  meta: { description: 'App skeleton' },
  children: [HomeRoute, LoginRoute]
}

export default AppRoute
