import requireAuthRoute from '@/utils/requireAuthRoute'
import DashboardRoute from '@/modules/Dashboard/router'
import BillCategorizeRoute from '@/modules/BillCategorize/router'
import BillDownloadRoute from '@/modules/BillDownload/router'
import BillRelevanceRoute from '@/modules/BillRelevance/router'
import BillManagementRoute from '@/modules/BillManagement/router'

const HomeLayout = () => import('@/modules/Home/views/Layout')

const HomeRoute = {
  path: '/',
  beforeEnter: requireAuthRoute,
  component: HomeLayout,
  name: 'HomeLayout',
  meta: { description: 'Home page' },
  children: [DashboardRoute, BillCategorizeRoute, BillDownloadRoute, BillRelevanceRoute, BillManagementRoute]
}

export default HomeRoute
