const DashboardLayout = () => import('@/modules/Dashboard/views/Layout')

const DashboardRoute = {
  path: 'dashboard',
  alias: '/',
  component: DashboardLayout,
  name: 'DashboardLayout',
  meta: { description: 'Volunteer site dashboard' }
}

export default DashboardRoute
